# Installation
> `npm install --save @types/gettext-parser`

# Summary
This package contains type definitions for gettext-parser (https://github.com/smhg/gettext-parser).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/gettext-parser.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/gettext-parser/index.d.ts)
````ts
// Type definitions for gettext-parser 4.0
// Project: https://github.com/smhg/gettext-parser
// Definitions by: Lorent Lempereur <https://github.com/looorent>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference types="node" />

export interface GetTextComment {
    translator: string;
    reference: string;
    extracted: string;
    flag: string;
    previous: string;
}

export interface GetTextTranslation {
    msgctxt?: string | undefined;
    msgid: string;
    msgid_plural?: any;
    msgstr: string[];
    comments?: GetTextComment | undefined;
}

export interface GetTextTranslations {
    charset: string;
    headers: { [headerName: string]: string };
    translations: { [msgctxt: string]: { [msgId: string]: GetTextTranslation } };
}

export interface PoParser {
    parse: (buffer: Buffer | string, defaultCharset?: string) => GetTextTranslations;
    compile: (table: GetTextTranslations, options?: any) => Buffer;
    createParseStream: (buffer: any, defaultCharset?: string) => any;
}

export interface MoParser {
    parse: (buffer: Buffer | string, defaultCharset?: string) => GetTextTranslations;
    compile: (table: GetTextTranslations, options?: any) => Buffer;
}

export const po: MoParser;
export const mo: MoParser;

````

### Additional Details
 * Last updated: Thu, 08 Jul 2021 12:02:04 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Lorent Lempereur](https://github.com/looorent).
